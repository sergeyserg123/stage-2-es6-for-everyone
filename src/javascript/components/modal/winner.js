import { showModal } from "./modal";
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  const title = `Fighter ${fighter.name} win!`;
  const imgElement = createFighterImage(fighter);
  
  showModal({title: title, bodyElement: imgElement});
}
