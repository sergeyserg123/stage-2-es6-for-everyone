import { controls } from '../../constants/controls';


export async function fight(firstFighter, secondFighter) {
  let {firstFighterState, secondFighterState } = setInitialStates(firstFighter, secondFighter);
    
  const hitsPowerEventsPromise = registerHitsPowerListeners(firstFighterState, secondFighterState);
  const blocksPowerEventsPromise = registerBlocksPowerListeners(firstFighterState, secondFighterState);
  const playerOneCriticalHitCombinationsPromise = registerPlayerOneCriticalHitCombinationsListeners(firstFighterState, secondFighterState);
  const playerTwoCriticalHitCombinationsPromise = registerPlayerTwoCriticalHitCombinationsListeners(firstFighterState, secondFighterState);

  Promise.all([
    hitsPowerEventsPromise, 
    blocksPowerEventsPromise,
    playerOneCriticalHitCombinationsPromise,
    playerTwoCriticalHitCombinationsPromise
  ]).then((a, b, c, d) => {
    console.log('unregistered all Listeners!')
  });

  return new Promise((resolve) => {
    document.addEventListener('winner', (e) => {
      const fighter =  e.detail.winner;
      resolve(fighter);
    });
  });
}


export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker) - getBlockPower(defender);
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  let criticalHitChance = calcChance();
  let hitPower = fighter.attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter) {
  let dodgeChance = calcChance();
  let blockPower = fighter.defense * dodgeChance;
  return blockPower;
}

function getCriticalDamage(attacker) {
  let damage = 2 * attacker.attack;
  return damage;
}

function calcChance() {
  return Math.random() + 1;
}

function registerHitsPowerListeners(firstFighterState, secondFighterState) {
  const onKeyDown = (event) => {
    let damage = 0;
      switch(event.code){
        case controls.PlayerOneAttack:
          if (!firstFighterState.isProtected && !secondFighterState.isProtected) {
  
            damage = getDamage(firstFighterState.fighterInfo, secondFighterState.fighterInfo);
   
            secondFighterState.fighterInfo.health -= damage;  
            updateHealthBar(secondFighterState, firstFighterState);  
          } 
          break;
  
        case controls.PlayerTwoAttack:
          if (!firstFighterState.isProtected && !secondFighterState.isProtected) {
            damage = getDamage(secondFighterState.fighterInfo, firstFighterState.fighterInfo);
            
            firstFighterState.fighterInfo.health -= damage;
            updateHealthBar(firstFighterState, secondFighterState);
          } 
          break;
      }
  };
  
  document.addEventListener('keydown', onKeyDown);

  return new Promise((resolve) => {
    document.addEventListener('deleteEvents', (e) => {
      document.removeEventListener('keydown', onKeyDown);
      resolve();
    });
  });
}

function registerBlocksPowerListeners(firstFighterState, secondFighterState) {

  const onKeyDown = (event) => {
    switch(event.code){

      case controls.PlayerOneBlock:
        if (!firstFighterState.isProtected) {
          firstFighterState.isProtected = true;
        }        
        break;

      case controls.PlayerTwoBlock:
        if (!secondFighterState.isProtected) {
          secondFighterState.isProtected = true;
        }  
        break;
    }
  };

  const onKeyUp = (event) => {
    switch(event.code){

      case controls.PlayerOneBlock:
        if(firstFighterState.isProtected) {
          firstFighterState.isProtected = false;
        }
        break;

      case controls.PlayerTwoBlock:
        if(secondFighterState.isProtected) {
          secondFighterState.isProtected = false;
        }
        break;
    }
  };

  document.addEventListener('keydown', onKeyDown);
  document.addEventListener('keyup', onKeyUp);

  return new Promise((resolve) => {
    document.addEventListener('deleteEvents', (e) => {
      document.removeEventListener('keydown', onKeyDown);
      document.removeEventListener('keyup', onKeyUp);
      resolve();
    });
  });
}

function registerPlayerOneCriticalHitCombinationsListeners(firstFighterState, secondFighterState) {
  let [KeyQ, KeyW, KeyE] = controls.PlayerOneCriticalHitCombination;

  let criticalCombination = new Set();
  let damage = 0;

  const onKeyDown = (event) => {   
    switch (event.code) {
      case KeyQ:
        if (firstFighterState.CriticalHitCombinationAvailable)
          criticalCombination.add(KeyQ);
        break;
      case KeyW:
        if (firstFighterState.CriticalHitCombinationAvailable)  
          criticalCombination.add(KeyW);
        break;
      case KeyE:
        if (firstFighterState.CriticalHitCombinationAvailable)
          criticalCombination.add(KeyE);
        break;
    }
    
    if (criticalCombination.size === controls.PlayerOneCriticalHitCombination.length) {
      damage = getCriticalDamage(firstFighterState.fighterInfo);
      secondFighterState.fighterInfo.health -= damage; 
      updateHealthBar(secondFighterState, firstFighterState);
      disableCriticalHitCombination(firstFighterState);
      criticalCombination.clear();
    } 
  };

  const onKeyUp = (event) => {
    switch (event.code) {
      case KeyQ:
        criticalCombination.delete(KeyQ);
        break;
      case KeyW:
        criticalCombination.delete(KeyW);
        break;
      case KeyE:
        criticalCombination.delete(KeyE);
        break;
    } 
  };

  document.addEventListener('keydown', onKeyDown);
  document.addEventListener('keyup', onKeyUp);

  return new Promise((resolve) => {
    document.addEventListener('deleteEvents', (e) => {
      document.removeEventListener('keydown', onKeyDown);
      document.removeEventListener('keyup', onKeyUp);
      resolve();
    });
  });
}

function registerPlayerTwoCriticalHitCombinationsListeners(firstFighterState, secondFighterState) {
  let [KeyU, KeyI, KeyO] = controls.PlayerTwoCriticalHitCombination;

  let criticalCombination = new Set();
  let damage = 0;

  const onKeyDown = (event) => {   
    switch (event.code) {
      case KeyU:
        if (secondFighterState.CriticalHitCombinationAvailable)
          criticalCombination.add(KeyU);
        break;
      case KeyI:
        if (secondFighterState.CriticalHitCombinationAvailable)  
          criticalCombination.add(KeyI);
        break;
      case KeyO:
        if (secondFighterState.CriticalHitCombinationAvailable)
          criticalCombination.add(KeyO);
        break;
    }
    
    if (criticalCombination.size === controls.PlayerTwoCriticalHitCombination.length) {
      damage = getCriticalDamage(secondFighterState.fighterInfo);
      firstFighterState.fighterInfo.health -= damage;
      updateHealthBar(firstFighterState, secondFighterState);
      disableCriticalHitCombination(secondFighterState);
      criticalCombination.clear();
    } 
  };

  const onKeyUp = (event) => {
    switch (event.code) {
      case KeyU:
        criticalCombination.delete(KeyU);
        break;
      case KeyI:
        criticalCombination.delete(KeyI);
        break;
      case KeyO:
        criticalCombination.delete(KeyO);
        break;
    } 
  };

  document.addEventListener('keydown', onKeyDown);
  document.addEventListener('keyup', onKeyUp);

  return new Promise((resolve) => {
    document.addEventListener('deleteEvents', (e) => {
      document.removeEventListener('keydown', onKeyDown);
      document.removeEventListener('keyup', onKeyUp);
      resolve();
    });
  });
}

function updateHealthBar(defenderState, attackerState) {
  let {position} = defenderState;
  let healthBar = document.getElementById(`${position}-fighter-indicator`);

  let fullHealthVal = defenderState.fullHealthVal;
  let restHealth = defenderState.fighterInfo.health;

  let result = (100 * restHealth) / fullHealthVal;

  healthBar.style.width = result <= 0 ? '0%' : `${result}%`;
  if (result <= 0) {
    dispatchWinner(attackerState.fighterInfo);
  } 
}

function dispatchWinner(winner) {
  const winnerCustomEvent = new CustomEvent('winner', { detail: {
    winner: winner
  }} );
  const deleteEventsCustomEvent = new CustomEvent('deleteEvents');
  document.dispatchEvent(winnerCustomEvent);
  document.dispatchEvent(deleteEventsCustomEvent);
}

function disableCriticalHitCombination(fighterState) {
  fighterState.CriticalHitCombinationAvailable = false;
  setTimeout(() => {
    fighterState.CriticalHitCombinationAvailable = true;
  }, 10000);
}

function setInitialStates(firstFighter, secondFighter) {
  let firstFighterState = {
    isProtected: false,
    criticalHitChance: 0,
    dodgeChance: 0,
    fullHealthVal: 0,
    CriticalHitCombinationAvailable: true,
    fighterInfo: null,
    position: 'left'
  };
  
  let secondFighterState = {
    isProtected: false,
    criticalHitChance: 0,
    dodgeChance: 0,
    fullHealthVal: 0,
    CriticalHitCombinationAvailable: true,
    fighterInfo: null,
    position: 'right'
  };

  firstFighterState = {...firstFighterState, fighterInfo: firstFighter, fullHealthVal: firstFighter.health};
  secondFighterState = {...secondFighterState, fighterInfo: secondFighter, fullHealthVal: secondFighter.health};

  return {firstFighterState, secondFighterState}
}